<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>exercici1</title>
</head>
    <body>
        <?php 
            
            function lanzardado() {
                return rand(1,6);
            }

            //Jugador 1 dados 
            $dado1_1=lanzardado();
            $dado1_2=lanzardado();
            $dado1_3=lanzardado();

            //Jugador 2 dados 
            $dado2_1=lanzardado();
            $dado2_2=lanzardado();
            $dado2_3=lanzardado();

            // variables auxiliares
            $trio1 = 0;
            $trio2 = 0;

            $pareja1 = 0;
            $pareja2 = 0;

            $total1 = 0;
            $total2 = 0;

            //ha obtenido un trío de dados iguales de mayor valor, si los dos han obtenido tríos distintos
            // verificamos si jugador 1 ha sacado trio
            if ($dado1_1 == $dado1_2 && $dado12 == $dado1_3) {
                $trio1 = $dado1_1;
            }
            // verificamos si jugador 2 ha sacado trio
            if ($dado2_1 == $dado2_2 && $dado2_2 == $dado2_3) {
                $trio1 = $dado2_1;
            }

            // Verificar si el jugador 1 ha obtenido pareja
            if ($dado1_1 == $dado1_2 || $dado1_2 == $dado1_3 || $dado1_1 == $dado1_3) {
                $pareja1 = max($dado1_1, $dado1_2, $dado1_3);
            }

            // Verificar si el jugador 2 ha obtenido pareja
            if ($dado2_1 == $dado2_2 || $dado2_2 == $dado2_3 || $dado2_1 == $dado2_3) {
                $pareja2 = max($dado2_1, $dado2_2, $dado2_3);
            }

            // Calcular la puntuación total para cada jugador
            $total1 = $dado1_1 + $dado1_2 + $dado1_3;
            $total2 = $dado2_1 + $dado2_2 + $dado2_3;

            //Jugador 1 dados 
            echo "<p>Jugador1</p>";
            echo "<img src='./daus/$dado1_1.png' >";
            echo "<img src='./daus/$dado1_2.png' >";
            echo "<img src='./daus/$dado1_3.png' >";


            //Jugador 2 dados 
            echo "<p>Jugador2</p>";
            echo "<img src='./daus/$dado2_1.png' >";
            echo "<img src='./daus/$dado2_2.png' >";
            echo "<img src='./daus/$dado2_3.png' >";
            echo "<p></p>";

            // Determinar quién ha ganado
            if ($trio1 > 0 && $trio2 > 0) {
                echo "Empate: ambos jugadores han obtenido trío.";
            } elseif ($trio1 > 0) {
                echo "Jugador 1 ha ganado con trío de valor $trio1.";
            } elseif ($trio2 > 0) {
                echo "Jugador 2 ha ganado con trío de valor $trio2.";
            } elseif ($pareja1 > 0 && $pareja2 > 0) {
                echo "Empate: ambos jugadores han obtenido pareja.";
            } elseif ($pareja1 > 0) {
                echo "Jugador 1 ha ganado con pareja de valor $pareja1.";
            } elseif ($pareja2 > 0) {
                echo "Jugador 2 ha ganado con pareja de valor $pareja2.";
            } elseif ($total1 == $total2) {
                echo "Empate: ambos jugadores tienen la misma puntuación total.";
            } elseif ($total1 > $total2) {
                echo "Jugador 1 ha ganado con una puntuación total de $total1.";
            } else {
                echo "Jugador 2 ha ganado con una puntuación total de $total2.";
            }




        ?>
    </body>
</html>


<!-- 2. Escriba un programa que cada vez que se ejecute muestre la tirada de dos jugadores que tiran cada uno tres dados al azar y diga quién ha ganado. (2 punts)

En este juego, gana el jugador que:

ha obtenido un trío de dados iguales de mayor valor, si los dos han obtenido tríos distintos
ha obtenido un trío de dados iguales, si el otro jugador no ha obtenido trío
ha obtenido una pareja de dados iguales de mayor valor, si los dos han obtenido parejas distintas
ha obtenido una puntuación total mayor, si los dos han obtenido la misma pareja
ha obtenido una pareja de dados iguales, si el otro jugador no ha obtenido pareja
ha obtenido la mayor puntuación total, si ningún jugador ha obtenido pareja
Si no gana ningún jugador, lógicamente se habrá producido un empate.

Ayuda
Variables auxiliares
Se pueden crear tres variables auxiliares para cada jugador:

$trio1 / $trio2, que guarden el valor del trío obtenido si se ha obtenido trío o 0 si no se ha obtenido trío
$pareja1 / $pareja2, que guarden el valor de la pareja obtenida si se ha obtenido pareja o 0 si no se ha obtenido pareja
$total1 / $total2, que guarden el total de puntos obtenidos
Comparando las variables auxiliares se puede determinar qué jugador ha ganado. -->