<!DOCTYPE html>
<html>
    <head>
        <title>Exemple </title>
    </head>
    <body>
        <?php

            echo $_SERVER['PHP_SELF']; //Returns the filename of the currently executing script
            echo "<br>";
            echo $_SERVER['SERVER_ADDR']; //Returns the IP address of the host server
            echo "<br>";
            echo $_SERVER['SERVER_NAME']; //Returns the name of the host server
            echo "<br>";
            echo $_SERVER['SCRIPT_NAME']; //Returns the path of the current script
            echo "<br>";
            echo $_SERVER['SERVER_SOFTWARE']; //Returns the server identification string
            echo "<br>";
        ?>
    </body>
    
</html>